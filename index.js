let http = require('http');

let port = 4000

let server = http.createServer(function(request, response){
	
	if(request.url == '/' && request.method == 'GET'){
		response.writeHead(200, {ContentType: 'application/json'})
		response.end('Welcome to Booking System')
	}
	if(request.url == '/profile' && request.method == 'GET'){
		response.writeHead(200, {ContentType: 'application/json'})
		response.end('Welcome to your profile!')
	}
	if(request.url == '/courses' && request.method == 'GET'){
		response.writeHead(200, {ContentType: 'application/json'})
		response.end("Here's our courses available")
	}

	if(request.url == '/addcourse' && request.method == 'POST'){
		response.writeHead(200, {ContentType: 'application/json'})
		response.end("Add a course to our resources")
	}
})

server.listen(port)

console.log(`Server is running on localhost: ${port}`)